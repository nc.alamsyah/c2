import { useReactMediaRecorder } from "react-media-recorder";
import { createFileName } from "use-react-screenshot";

const Recorder = () => {
  const {
    status,
    startRecording,
    stopRecording,
    pauseRecording,
    resumeRecording,
    mediaBlobUrl,
  } = useReactMediaRecorder({ screen: true });
  const download = (
    video,
    { name = "ResultRecording", extension = "mp4" } = {}
  ) => {
    const a = document.createElement("a");
    a.href = mediaBlobUrl;
    a.download = createFileName(extension, name);
    a.click();
  };

  return (
    <div>
      <p className="text-lg">Status : {status}</p>
      <button className="w-28 h-8 bg-red-600" onClick={startRecording}>
        Start Recording
      </button>
      <button className="btn stopRecord" onClick={stopRecording}>
        Stop Recording
      </button>
      <button className="btn pauseRecord" onClick={pauseRecording}>
        Pause Recording
      </button>
      <button className="btn resumeRecord" onClick={resumeRecording}>
        Resume Recording
      </button>
      <div>
        {mediaBlobUrl && (
          <button className="btn" onClick={download}>
            Download Video Record
          </button>
        )}
      </div>
    </div>
  );
};

export default Recorder;
